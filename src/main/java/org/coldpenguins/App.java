package org.coldpenguins;

import java.io.IOException;
import java.util.List;

public class App {

	public static void main(String[] args) throws IOException {
		CustomerParser customerParser = new CustomerParser();

		List<Customer> customers = List.of(
				customerParser.parse("JSON"),
				customerParser.parse("YAML"),
				customerParser.parse("CSV")
		);

		for (Customer customer : customers) {
			System.out.println(customer.toString());
		}
	}
}
