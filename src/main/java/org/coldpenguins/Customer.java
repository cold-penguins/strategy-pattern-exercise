package org.coldpenguins;

public class Customer {

	private String firstName;

	private String lastName;

	private int age;

	private String city;

	private String address;

	private int houseNumber;

	private int postalCode;

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setPostalCode(int postalCode) {
		this.postalCode = postalCode;
	}

	public void setHouseNumber(int houseNumber) {
		this.houseNumber = houseNumber;
	}

	@Override
	public String toString() {
		return String.format(
				"Customer{firstName: %s | lastName: %s | age: %d | city: %s | address: %s | houseNumber: %d | postalCode: %d}",
				firstName,
				lastName,
				age,
				city,
				address,
				houseNumber,
				postalCode
		);
	}
}
