package org.coldpenguins;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.io.InputStream;

public class CustomerParser {

	public Customer parse(String fileType) throws IOException {
		Customer customer = null;

		if (fileType.equalsIgnoreCase("JSON")) {
			ObjectMapper mapper = new ObjectMapper(new JsonFactory());
			ClassLoader classLoader = getClass().getClassLoader();

			try (InputStream stream = classLoader.getResourceAsStream("customer.json")) {
				if (stream != null) {
					customer = mapper.readValue(stream, Customer.class);
				}
			}
		} else if (fileType.equalsIgnoreCase("YAML")) {
			ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
			ClassLoader classLoader = getClass().getClassLoader();

			try (InputStream stream = classLoader.getResourceAsStream("customer.yaml")) {
				if (stream != null) {
					customer = mapper.readValue(stream, Customer.class);
				}
			}
		} else if (fileType.equalsIgnoreCase("CSV")) {
			CsvSchema schema = CsvSchema.builder().setUseHeader(true).setColumnSeparator(';').build();
			CsvMapper mapper = new CsvMapper();

			ClassLoader classLoader = getClass().getClassLoader();

			try (InputStream stream = classLoader.getResourceAsStream("customer.csv")) {
				if (stream != null) {
					customer = mapper.readerFor(Customer.class).with(schema).readValue(stream);
				}
			}
		}

		return customer;
	}
}
