# Strategy Pattern Exercise

## Description

In this exercise you will need to refactor a class, which parses customer data from different file types.
The goal is to use the strategy pattern to make the parsing more readable, more maintainable and to remove code duplication.

## Using Instructions

To start with the exercise please import the project into your IDE or editor of choice.
Most IDEs and editor will set up the Gradle project automatically.

If you are using an environment without Gradle support you can execute the following command to build and start the project:
- Linux/Mac: `./gradlew run` or `./gradlew build`
- Windows: `gradlew.bat run` or `gradlew.bat build`

If you want to see a possible solution you can check out the branch `exercise-solution`.
